<?php

namespace Lerp\Equipment;

use Laminas\Router\Http\Literal;
use Lerp\Equipment\Controller\Ajax\Equipment\EquipmentAjaxController;
use Lerp\Equipment\Controller\Ajax\Equipment\EquipmentGroupAjaxController;
use Lerp\Equipment\Controller\Ajax\Equipment\EquipmentListsAjaxController;
use Lerp\Equipment\Controller\Ajax\User\EquipmentUserController;
use Lerp\Equipment\Controller\Rest\Equipment\AbsenceReasonRestController;
use Lerp\Equipment\Controller\Rest\Equipment\AbsenceRestController;
use Lerp\Equipment\Controller\Rest\Equipment\EquipmentRestController;
use Lerp\Equipment\Controller\Rest\Equipment\EquipmentUserRestController;
use Lerp\Equipment\Controller\Rest\User\UserEquipRestController;
use Lerp\Equipment\Factory\Controller\Ajax\Equipment\EquipmentAjaxControllerFactory;
use Lerp\Equipment\Factory\Controller\Ajax\Equipment\EquipmentGroupAjaxControllerFactory;
use Lerp\Equipment\Factory\Controller\Ajax\Equipment\EquipmentListsAjaxControllerFactory;
use Lerp\Equipment\Factory\Controller\Ajax\User\EquipmentUserControllerFactory;
use Lerp\Equipment\Factory\Controller\Rest\Equipment\AbsenceReasonRestControllerFactory;
use Lerp\Equipment\Factory\Controller\Rest\Equipment\AbsenceRestControllerFactory;
use Lerp\Equipment\Factory\Controller\Rest\Equipment\EquipmentRestControllerFactory;
use Lerp\Equipment\Factory\Controller\Rest\Equipment\EquipmentUserRestControllerFactory;
use Lerp\Equipment\Factory\Controller\Rest\User\UserEquipRestControllerFactory;
use Lerp\Equipment\Factory\Form\Equipment\AbsenceFormFactory;
use Lerp\Equipment\Factory\Form\Equipment\EquipmentFormFactory;
use Lerp\Equipment\Factory\Form\Equipment\UserDetailsFormFactory;
use Lerp\Equipment\Factory\Service\Equipment\AbsenceServiceFactory;
use Lerp\Equipment\Factory\Service\Equipment\EquipmentServiceFactory;
use Lerp\Equipment\Factory\Service\User\UserEquipServiceFactory;
use Lerp\Equipment\Factory\Table\Equipment\EquipmentAbsenceReasonTableFactory;
use Lerp\Equipment\Factory\Table\Equipment\EquipmentAbsenceTableFactory;
use Lerp\Equipment\Factory\Table\Equipment\EquipmentGroupRelTableFactory;
use Lerp\Equipment\Factory\Table\Equipment\EquipmentGroupTableFactory;
use Lerp\Equipment\Factory\Table\Equipment\EquipmentTableFactory;
use Lerp\Equipment\Factory\Table\Equipment\ViewAbsenceTableFactory;
use Lerp\Equipment\Factory\Table\User\EquipmentUserDetailsTableFactory;
use Lerp\Equipment\Factory\Table\User\EquipmentUserTableFactory;
use Lerp\Equipment\Factory\Table\User\ViewUserEquipTableFactory;
use Lerp\Equipment\Factory\Table\User\ViewUserRightRelationTableFactory;
use Lerp\Equipment\Form\Equipment\AbsenceForm;
use Lerp\Equipment\Form\Equipment\EquipmentForm;
use Lerp\Equipment\Form\Equipment\UserDetailsForm;
use Lerp\Equipment\Service\Equipment\AbsenceService;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceReasonTable;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceTable;
use Lerp\Equipment\Table\Equipment\EquipmentGroupRelTable;
use Lerp\Equipment\Table\Equipment\EquipmentGroupTable;
use Lerp\Equipment\Table\Equipment\EquipmentTable;
use Lerp\Equipment\Table\Equipment\ViewAbsenceTable;
use Lerp\Equipment\Table\User\EquipmentUserDetailsTable;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Laminas\Router\Http\Segment;
use Lerp\Equipment\Table\User\ViewUserEquipTable;
use Bitkorn\User\Table\User\ViewUserRightRelationTable;

return [
    'router'             => [
        'routes' => [
            /*
             * REST - user
             */
            'lerp_equipment_rest_user_equipmentuser'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-rest-user[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => UserEquipRestController::class,
                    ],
                ],
            ],
            /*
             * REST - equipment
             */
            'lerp_equipment_rest_equipment_equipment'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-rest-equipment[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EquipmentRestController::class,
                    ],
                ],
            ],
            /*
             * REST - equipmentUser
             */
            'lerp_equipment_rest_equipment_equipmentuser'                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-rest-equipment-user[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EquipmentUserRestController::class,
                    ],
                ],
            ],
            /*
             * REST - absence
             */
            'lerp_equipment_rest_equipment_absence'                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-rest-absence[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => AbsenceRestController::class,
                    ],
                ],
            ],
            /*
             * REST - absence reason
             */
            'lerp_equipment_rest_equipment_absencereason'                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-rest-absence-reason[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => AbsenceReasonRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX
             */
            'lerp_equipment_ajax_user_meuserdetails'                       => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-equipment-ajax-user-me-details',
                    'defaults' => [
                        'controller' => EquipmentUserController::class,
                        'action'     => 'meUserDetails'
                    ],
                ],
            ],
            'lerp_equipment_ajax_user_users'                               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-equipment-ajax-users',
                    'defaults' => [
                        'controller' => EquipmentUserController::class,
                        'action'     => 'users'
                    ],
                ],
            ],
            'lerp_equipment_ajax_equipment_lists_equipmentgroupassoc'      => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-equipment-ajax-lists-equipment-group-assoc',
                    'defaults' => [
                        'controller' => EquipmentListsAjaxController::class,
                        'action'     => 'equipmentGroupAssoc'
                    ],
                ],
            ],
            'lerp_equipment_ajax_equipment_lists_equipmentgroupswithuuids' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-ajax-lists-equipment-groups-with-uuids[/:uuids]',
                    'constraints' => [
                        'uuids' => '[0-9A-Fa-f,-]+',
                    ],
                    'defaults'    => [
                        'controller' => EquipmentListsAjaxController::class,
                        'action'     => 'equipmentGroupsWithUuids'
                    ],
                ],
            ],
            'lerp_equipment_ajax_equipment_lists_equipmentgroups'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-equipment-ajax-lists-equipment-groups',
                    'defaults' => [
                        'controller' => EquipmentListsAjaxController::class,
                        'action'     => 'equipmentGroups'
                    ],
                ],
            ],
            'lerp_equipment_ajax_equipment_equipmentgroupsforequipment'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-ajax-equipment-groups-equip/:equip_uuid',
                    'constraints' => [
                        'equip_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EquipmentAjaxController::class,
                        'action'     => 'userEquipmentGroups'
                    ],
                ],
            ],
            'lerp_equipment_ajax_equipmentgroup_toggleworkflowrel'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-equipment-ajax-equipmentgroup-toggle-workflow/:equip_group_uuid/:workflow_uuid',
                    'constraints' => [
                        'equip_group_uuid' => '[0-9A-Fa-f-]+',
                        'workflow_uuid'    => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EquipmentGroupAjaxController::class,
                        'action'     => 'toggleWorkflowRelation'
                    ],
                ],
            ],
        ],
    ],
    'controllers'        => [
        'factories'  => [
            // AJAX
            EquipmentUserController::class      => EquipmentUserControllerFactory::class,
            EquipmentAjaxController::class      => EquipmentAjaxControllerFactory::class,
            EquipmentListsAjaxController::class => EquipmentListsAjaxControllerFactory::class,
            EquipmentGroupAjaxController::class => EquipmentGroupAjaxControllerFactory::class,
            // REST
            UserEquipRestController::class      => UserEquipRestControllerFactory::class,
            EquipmentRestController::class      => EquipmentRestControllerFactory::class,
            AbsenceRestController::class        => AbsenceRestControllerFactory::class,
            AbsenceReasonRestController::class  => AbsenceReasonRestControllerFactory::class,
            EquipmentUserRestController::class  => EquipmentUserRestControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager'    => [
        'factories'  => [
            // service
            UserEquipService::class            => UserEquipServiceFactory::class,
            EquipmentService::class            => EquipmentServiceFactory::class,
            AbsenceService::class              => AbsenceServiceFactory::class,
            // table
            EquipmentUserTable::class          => EquipmentUserTableFactory::class,
            EquipmentUserDetailsTable::class   => EquipmentUserDetailsTableFactory::class,
            ViewUserEquipTable::class          => ViewUserEquipTableFactory::class,
            ViewUserRightRelationTable::class  => ViewUserRightRelationTableFactory::class,
            EquipmentTable::class              => EquipmentTableFactory::class,
            EquipmentAbsenceTable::class       => EquipmentAbsenceTableFactory::class,
            EquipmentAbsenceReasonTable::class => EquipmentAbsenceReasonTableFactory::class,
            EquipmentGroupTable::class         => EquipmentGroupTableFactory::class,
            EquipmentGroupRelTable::class      => EquipmentGroupRelTableFactory::class,
            ViewAbsenceTable::class            => ViewAbsenceTableFactory::class,
            // form
            AbsenceForm::class                 => AbsenceFormFactory::class,
            UserDetailsForm::class             => UserDetailsFormFactory::class,
            EquipmentForm::class               => EquipmentFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
    ],
    'view_manager'       => [
        'template_map'        => [
            'template/equipmentUnlockRequest' => __DIR__ . '/../view/template/equipmentUnlockRequest.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
];
