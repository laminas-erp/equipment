<?php

namespace Lerp\Equipment\Entity\User;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ViewUserRightRelationEntity extends AbstractEntity
{
    public array $mapping = [
        'user_right_relation_uuid'  => 'user_right_relation_uuid',
        'user_uuid'                 => 'user_uuid',
        'user_right_id'             => 'user_right_id',
        'user_right_relation_value' => 'user_right_relation_value',
        'user_right_alias'          => 'user_right_alias',
        'user_right_desc'           => 'user_right_desc',
        'user_login'                => 'user_login',
        'user_active'               => 'user_active',
        'user_email'                => 'user_email',
        'equipment_uuid'            => 'equipment_uuid',
        'user_lang_iso'             => 'user_lang_iso',
        'user_details_name_first'   => 'user_details_name_first',
        'user_details_name_last'    => 'user_details_name_last',
        'user_details_tel_land'     => 'user_details_tel_land',
        'user_details_tel_mobile'   => 'user_details_tel_mobile',
        'user_details_tel_fax'      => 'user_details_tel_fax',
        'user_details_brand_pre'    => 'user_details_brand_pre',
        'user_details_brand_post'   => 'user_details_brand_post',
        'user_details_job'          => 'user_details_job',
    ];

    public function getUserRightRelationUuid(): string
    {
        if (!isset($this->storage['user_right_relation_uuid'])) {
            return '';
        }
        return $this->storage['user_right_relation_uuid'];
    }

    public function setUserRightRelationUuid(string $userRightRelationUuid): void
    {
        $this->storage['user_right_relation_uuid'] = $userRightRelationUuid;
    }

    public function getUserUuid(): string
    {
        if (!isset($this->storage['user_uuid'])) {
            return '';
        }
        return $this->storage['user_uuid'];
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->storage['user_uuid'] = $userUuid;
    }

    public function getUserRightId(): int
    {
        if (!isset($this->storage['user_right_id'])) {
            return 0;
        }
        return $this->storage['user_right_id'];
    }

    public function setUserRightId(int $userRightId): void
    {
        $this->storage['user_right_id'] = $userRightId;
    }

    public function getUserRightRelationValue(): int
    {
        if (!isset($this->storage['user_right_relation_value'])) {
            return 0;
        }
        return $this->storage['user_right_relation_value'];
    }

    public function setUserRightRelationValue(int $userRightRelationValue): void
    {
        $this->storage['user_right_relation_value'] = $userRightRelationValue;
    }

    public function getUserRightAlias(): string
    {
        if (!isset($this->storage['user_right_alias'])) {
            return '';
        }
        return $this->storage['user_right_alias'];
    }

    public function setUserRightAlias(string $userRightAlias): void
    {
        $this->storage['user_right_alias'] = $userRightAlias;
    }

    public function getUserRightDesc(): string
    {
        if (!isset($this->storage['user_right_desc'])) {
            return '';
        }
        return $this->storage['user_right_desc'];
    }

    public function setUserRightDesc(string $userRightDesc): void
    {
        $this->storage['user_right_desc'] = $userRightDesc;
    }

    public function getUserLogin(): string
    {
        if (!isset($this->storage['user_login'])) {
            return '';
        }
        return $this->storage['user_login'];
    }

    public function setUserLogin(string $userLogin): void
    {
        $this->storage['user_login'] = $userLogin;
    }

    public function getUserActive(): int
    {
        if (!isset($this->storage['user_active'])) {
            return 0;
        }
        return $this->storage['user_active'];
    }

    public function setUserActive(int $userActive): void
    {
        $this->storage['user_active'] = $userActive;
    }

    public function getUserEmail(): string
    {
        if (!isset($this->storage['user_email'])) {
            return '';
        }
        return $this->storage['user_email'];
    }

    public function setUserEmail(string $userEmail): void
    {
        $this->storage['user_email'] = $userEmail;
    }

    public function getEquipmentUuid(): string
    {
        if (!isset($this->storage['equipment_uuid'])) {
            return '';
        }
        return $this->storage['equipment_uuid'];
    }

    public function setEquipmentUuid(string $equipmentUuid): void
    {
        $this->storage['equipment_uuid'] = $equipmentUuid;
    }

    public function getUserLangIso(): string
    {
        if (!isset($this->storage['user_lang_iso'])) {
            return '';
        }
        return $this->storage['user_lang_iso'];
    }

    public function setUserLangIso(string $userLangIso): void
    {
        $this->storage['user_lang_iso'] = $userLangIso;
    }

    public function getUserDetailsNameFirst(): string
    {
        if (!isset($this->storage['user_details_name_first'])) {
            return '';
        }
        return $this->storage['user_details_name_first'];
    }

    public function setUserDetailsNameFirst(string $userDetailsNameFirst): void
    {
        $this->storage['user_details_name_first'] = $userDetailsNameFirst;
    }

    public function getUserDetailsNameLast(): string
    {
        if (!isset($this->storage['user_details_name_last'])) {
            return '';
        }
        return $this->storage['user_details_name_last'];
    }

    public function setUserDetailsNameLast(string $userDetailsNameLast): void
    {
        $this->storage['user_details_name_last'] = $userDetailsNameLast;
    }

    public function getUserDetailsTelLand(): string
    {
        if (!isset($this->storage['user_details_tel_land'])) {
            return '';
        }
        return $this->storage['user_details_tel_land'];
    }

    public function setUserDetailsTelLand(string $userDetailsTelLand): void
    {
        $this->storage['user_details_tel_land'] = $userDetailsTelLand;
    }

    public function getUserDetailsTelMobile(): string
    {
        if (!isset($this->storage['user_details_tel_mobile'])) {
            return '';
        }
        return $this->storage['user_details_tel_mobile'];
    }

    public function setUserDetailsTelMobile(string $userDetailsTelMobile): void
    {
        $this->storage['user_details_tel_mobile'] = $userDetailsTelMobile;
    }

    public function getUserDetailsTelFax(): string
    {
        if (!isset($this->storage['user_details_tel_fax'])) {
            return '';
        }
        return $this->storage['user_details_tel_fax'];
    }

    public function setUserDetailsTelFax(string $userDetailsTelFax): void
    {
        $this->storage['user_details_tel_fax'] = $userDetailsTelFax;
    }

    public function getUserDetailsBrandPre(): string
    {
        if (!isset($this->storage['user_details_brand_pre'])) {
            return '';
        }
        return $this->storage['user_details_brand_pre'];
    }

    public function setUserDetailsBrandPre(string $userDetailsBrandPre): void
    {
        $this->storage['user_details_brand_pre'] = $userDetailsBrandPre;
    }

    public function getUserDetailsBrandPost(): string
    {
        if (!isset($this->storage['user_details_brand_post'])) {
            return '';
        }
        return $this->storage['user_details_brand_post'];
    }

    public function setUserDetailsBrandPost(string $userDetailsBrandPost): void
    {
        $this->storage['user_details_brand_post'] = $userDetailsBrandPost;
    }

    public function getUserDetailsJob(): string
    {
        if (!isset($this->storage['user_details_job'])) {
            return '';
        }
        return $this->storage['user_details_job'];
    }

    public function setUserDetailsJob(string $userDetailsJob): void
    {
        $this->storage['user_details_job'] = $userDetailsJob;
    }
}
