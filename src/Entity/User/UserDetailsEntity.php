<?php

namespace Lerp\Equipment\Entity\User;

use Bitkorn\Trinket\Entity\AbstractEntity;

class UserDetailsEntity extends AbstractEntity
{
    public array $mapping = [
        'user_uuid'               => 'user_uuid',
        'user_details_name_first' => 'user_details_name_first',
        'user_details_name_last'  => 'user_details_name_last',
        'user_details_tel_land'   => 'user_details_tel_land',
        'user_details_tel_mobile' => 'user_details_tel_mobile',
        'user_details_tel_fax'    => 'user_details_tel_fax',
        'user_details_brand_pre'  => 'user_details_brand_pre',
        'user_details_brand_post' => 'user_details_brand_post',
        'user_details_job'        => 'user_details_job',
    ];

    protected $primaryKey = 'user_uuid';

    public function getUserUuid(): string
    {
        if (!isset($this->storage['user_uuid'])) {
            return '';
        }
        return $this->storage['user_uuid'];
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->storage['user_uuid'] = $userUuid;
    }

    public function getUserDetailsNameFirst(): string
    {
        if (!isset($this->storage['user_details_name_first'])) {
            return '';
        }
        return $this->storage['user_details_name_first'];
    }

    public function setUserDetailsNameFirst(string $userDetailsNameFirst): void
    {
        $this->storage['user_details_name_first'] = $userDetailsNameFirst;
    }

    public function getUserDetailsNameLast(): string
    {
        if (!isset($this->storage['user_details_name_last'])) {
            return '';
        }
        return $this->storage['user_details_name_last'];
    }

    public function setUserDetailsNameLast(string $userDetailsNameLast): void
    {
        $this->storage['user_details_name_last'] = $userDetailsNameLast;
    }

    public function getUserDetailsTelLand(): string
    {
        if (!isset($this->storage['user_details_tel_land'])) {
            return '';
        }
        return $this->storage['user_details_tel_land'];
    }

    public function setUserDetailsTelLand(string $userDetailsTelLand): void
    {
        $this->storage['user_details_tel_land'] = $userDetailsTelLand;
    }

    public function getUserDetailsTelMobile(): string
    {
        if (!isset($this->storage['user_details_tel_mobile'])) {
            return '';
        }
        return $this->storage['user_details_tel_mobile'];
    }

    public function setUserDetailsTelMobile(string $userDetailsTelMobile): void
    {
        $this->storage['user_details_tel_mobile'] = $userDetailsTelMobile;
    }

    public function getUserDetailsTelFax(): string
    {
        if (!isset($this->storage['user_details_tel_fax'])) {
            return '';
        }
        return $this->storage['user_details_tel_fax'];
    }

    public function setUserDetailsTelFax(string $userDetailsTelFax): void
    {
        $this->storage['user_details_tel_fax'] = $userDetailsTelFax;
    }

    public function getUserDetailsBrandPre(): string
    {
        if (!isset($this->storage['user_details_brand_pre'])) {
            return '';
        }
        return $this->storage['user_details_brand_pre'];
    }

    public function setUserDetailsBrandPre(string $userDetailsBrandPre): void
    {
        $this->storage['user_details_brand_pre'] = $userDetailsBrandPre;
    }

    public function getUserDetailsBrandPost(): string
    {
        if (!isset($this->storage['user_details_brand_post'])) {
            return '';
        }
        return $this->storage['user_details_brand_post'];
    }

    public function setUserDetailsBrandPost(string $userDetailsBrandPost): void
    {
        $this->storage['user_details_brand_post'] = $userDetailsBrandPost;
    }

    public function getUserDetailsJob(): string
    {
        if (!isset($this->storage['user_details_job'])) {
            return '';
        }
        return $this->storage['user_details_job'];
    }

    public function setUserDetailsJob(string $userDetailsJob): void
    {
        $this->storage['user_details_job'] = $userDetailsJob;
    }
}
