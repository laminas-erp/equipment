<?php

namespace Lerp\Equipment\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Select;

class ParamsUserEquip extends ParamsBase
{
    protected array $orderFieldsAvailable = [
        'user_login', 'user_active', 'user_time_create', 'user_lang_iso'
        , 'user_details_name_first', 'user_details_name_last'
        , 'user_details_job', 'user_role_id'
    ];

    protected FilterChainStringSanitize $filterChainStringSanitize;

    protected bool $onlyEquip;
    protected string $userGroupAlias;

    public function __construct()
    {
        parent::__construct();
        $this->filterChainStringSanitize = new FilterChainStringSanitize();
    }

    public function isOnlyEquip(): bool
    {
        return $this->onlyEquip;
    }

    public function setOnlyEquip(bool $onlyEquip): void
    {
        $this->onlyEquip = $onlyEquip;
    }

    public function setUserGroupAlias(string $userGroupAlias): void
    {
        $this->userGroupAlias = $this->filterChainStringSanitize->filter($userGroupAlias);
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setOnlyEquip(isset($qp['only_equip']) && $qp['only_equip'] == 'true');
        $this->setUserGroupAlias($qp['group_alias'] ?? '');
    }

    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select, $orderDefault);
        if (!empty($this->userGroupAlias)) {
            $selectGroup = new Select('user_group');
            $selectGroup->columns(['user_group_uuid']);
            $selectGroup->where(['user_group_alias' => $this->userGroupAlias]);

            $selectRelation = new Select('user_group_relation');
            $selectRelation->columns(['user_uuid']);
            $selectRelation->where->in('user_group_uuid', $selectGroup);

            $select->where->in('user.user_uuid', $selectRelation);
        }
    }

}
