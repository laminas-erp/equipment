<?php

namespace Lerp\Equipment\Entity\Equipment;

use Bitkorn\Trinket\Entity\AbstractEntity;

class EquipmentAbsenceEntity extends AbstractEntity
{
    public array $mapping = [
        'equipment_absence_uuid'        => 'equipment_absence_uuid',
        'equipment_uuid'                => 'equipment_uuid',
        'equipment_no'                  => 'equipment_no',
        'equipment_absence_start'       => 'equipment_absence_start',
        'equipment_absence_end'         => 'equipment_absence_end',
        'equipment_absence_reason_uuid' => 'equipment_absence_reason_uuid',
        'equipment_absence_subject'     => 'equipment_absence_subject',
        'equipment_absence_text'        => 'equipment_absence_text',
    ];

    protected $primaryKey = 'equipment_absence_uuid';

    public function getEquipmentAbsenceUuid(): string
    {
        if (!isset($this->storage['equipment_absence_uuid'])) {
            return '';
        }
        return $this->storage['equipment_absence_uuid'];
    }

    public function setEquipmentAbsenceUuid(string $equipmentAbsenceUuid): void
    {
        $this->storage['equipment_absence_uuid'] = $equipmentAbsenceUuid;
    }

    public function getEquipmentUuid(): string
    {
        if (!isset($this->storage['equipment_uuid'])) {
            return '';
        }
        return $this->storage['equipment_uuid'];
    }

    public function setEquipmentUuid(string $equipmentUuid): void
    {
        $this->storage['equipment_uuid'] = $equipmentUuid;
    }

    public function getEquipmentNo(): int
    {
        if (!isset($this->storage['equipment_no'])) {
            return 0;
        }
        return $this->storage['equipment_no'];
    }

    public function setEquipmentNo(int $equipmentNo): void
    {
        $this->storage['equipment_no'] = $equipmentNo;
    }

    public function getEquipmentAbsenceStart(): string
    {
        if (!isset($this->storage['equipment_absence_start'])) {
            return '';
        }
        return $this->storage['equipment_absence_start'];
    }

    public function setEquipmentAbsenceStart(string $equipmentAbsenceStart): void
    {
        $this->storage['equipment_absence_start'] = $equipmentAbsenceStart;
    }

    public function getEquipmentAbsenceEnd(): string
    {
        if (!isset($this->storage['equipment_absence_end'])) {
            return '';
        }
        return $this->storage['equipment_absence_end'];
    }

    public function setEquipmentAbsenceEnd(string $equipmentAbsenceEnd): void
    {
        $this->storage['equipment_absence_end'] = $equipmentAbsenceEnd;
    }

    public function getEquipmentAbsenceReasonUuid(): string
    {
        if (!isset($this->storage['equipment_absence_reason_uuid'])) {
            return '';
        }
        return $this->storage['equipment_absence_reason_uuid'];
    }

    public function setEquipmentAbsenceReasonUuid(string $equipmentAbsenceReasonUuid): void
    {
        $this->storage['equipment_absence_reason_uuid'] = $equipmentAbsenceReasonUuid;
    }

    public function getEquipmentAbsenceSubject(): string
    {
        if (!isset($this->storage['equipment_absence_subject'])) {
            return '';
        }
        return $this->storage['equipment_absence_subject'];
    }

    public function setEquipmentAbsenceSubject(string $equipmentAbsenceSubject): void
    {
        $this->storage['equipment_absence_subject'] = $equipmentAbsenceSubject;
    }

    public function getEquipmentAbsenceText(): string
    {
        if (!isset($this->storage['equipment_absence_text'])) {
            return '';
        }
        return $this->storage['equipment_absence_text'];
    }

    public function setEquipmentAbsenceText(string $equipmentAbsenceText): void
    {
        $this->storage['equipment_absence_text'] = $equipmentAbsenceText;
    }
}
