<?php

namespace Lerp\Equipment\Entity\Equipment;

use Bitkorn\Trinket\Entity\AbstractEntity;

class EquipmentEntity extends AbstractEntity
{
    public array $mapping = [
        'equipment_uuid'           => 'equipment_uuid',
        'equipment_no'             => 'equipment_no',
        'equipment_price_per_hour' => 'equipment_price_per_hour',
        'location_place_uuid'      => 'location_place_uuid',
        //
        'equipment_groups_csv'     => 'equipment_groups_csv',
    ];

    protected $primaryKey = 'equipment_uuid';

    public function getEquipmentUuid(): string
    {
        if (!isset($this->storage['equipment_uuid'])) {
            return '';
        }
        return $this->storage['equipment_uuid'];
    }

    public function setEquipmentUuid(string $equipmentUuid): void
    {
        $this->storage['equipment_uuid'] = $equipmentUuid;
    }

    public function getEquipmentNo(): int
    {
        if (!isset($this->storage['equipment_no'])) {
            return 0;
        }
        return $this->storage['equipment_no'];
    }

    public function setEquipmentNo(int $equipmentNo): void
    {
        $this->storage['equipment_no'] = $equipmentNo;
    }

    public function getEquipmentPricePerHour(): float
    {
        if (!isset($this->storage['equipment_price_per_hour'])) {
            return '';
        }
        return $this->storage['equipment_price_per_hour'];
    }

    public function setEquipmentPricePerHour(float $equipmentPricePerHour): void
    {
        $this->storage['equipment_price_per_hour'] = $equipmentPricePerHour;
    }

    public function getLocationPlaceUuid(): string
    {
        if (!isset($this->storage['location_place_uuid'])) {
            return '';
        }
        return $this->storage['location_place_uuid'];
    }

    public function setLocationPlaceUuid(string $locationPlaceUuid): void
    {
        $this->storage['location_place_uuid'] = $locationPlaceUuid;
    }

    public function getEquipmentGroupsCsv(): string
    {
        if (!isset($this->storage['equipment_groups_csv'])) {
            return '';
        }
        return $this->storage['equipment_groups_csv'];
    }

    public function getEquipmentGroupsCsvAsArray(): array
    {
        if (!isset($this->storage['equipment_groups_csv'])) {
            return [];
        }
        return explode(',', $this->storage['equipment_groups_csv']);
    }

    public function setEquipmentGroupsCsv(string $equipmentGroupsCsv): void
    {
        $this->storage['equipment_groups_csv'] = $equipmentGroupsCsv;
    }
}
