<?php

namespace Lerp\Equipment\Factory\Controller\Rest\Equipment;

use Bitkorn\User\Form\User\UserForm;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Controller\Rest\Equipment\EquipmentUserRestController;
use Lerp\Equipment\Form\Equipment\EquipmentForm;
use Lerp\Equipment\Form\Equipment\UserDetailsForm;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Service\User\UserEquipService;

class EquipmentUserRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new EquipmentUserRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setUserForm($container->get(UserForm::class));
        $controller->setUserDetailsForm($container->get(UserDetailsForm::class));
        $controller->setEquipmentForm($container->get(EquipmentForm::class));
        $controller->setUserEquipService($container->get(UserEquipService::class));
        $controller->setEquipmentService($container->get(EquipmentService::class));
        return $controller;
    }
}
