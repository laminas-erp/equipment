<?php

namespace Lerp\Equipment\Factory\Controller\Rest\Equipment;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Controller\Rest\Equipment\AbsenceRestController;
use Lerp\Equipment\Form\Equipment\AbsenceForm;
use Lerp\Equipment\Service\Equipment\AbsenceService;

class AbsenceRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AbsenceRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setAbsenceService($container->get(AbsenceService::class));
        $controller->setAbsenceForm($container->get(AbsenceForm::class));
        return $controller;
    }
}
