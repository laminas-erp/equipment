<?php

namespace Lerp\Equipment\Factory\Controller\Rest\User;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Equipment\Controller\Rest\User\UserEquipRestController;
use Lerp\Equipment\Service\User\UserEquipService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserEquipRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new UserEquipRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setEquipmentUserService($container->get(UserEquipService::class));
        return $controller;
    }
}
