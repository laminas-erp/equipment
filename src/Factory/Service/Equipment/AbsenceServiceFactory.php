<?php

namespace Lerp\Equipment\Factory\Service\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Service\Equipment\AbsenceService;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceReasonTable;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceTable;
use Lerp\Equipment\Table\Equipment\ViewAbsenceTable;

class AbsenceServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new AbsenceService();
        $service->setLogger($container->get('logger'));
        $service->setEquipmentAbsenceTable($container->get(EquipmentAbsenceTable::class));
        $service->setEquipmentAbsenceReasonTable($container->get(EquipmentAbsenceReasonTable::class));
        $service->setViewAbsenceTable($container->get(ViewAbsenceTable::class));
        return $service;
    }
}
