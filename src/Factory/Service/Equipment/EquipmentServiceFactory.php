<?php

namespace Lerp\Equipment\Factory\Service\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Table\Equipment\EquipmentGroupRelTable;
use Lerp\Equipment\Table\Equipment\EquipmentGroupTable;
use Lerp\Equipment\Table\Equipment\EquipmentTable;
use Lerp\Product\Table\Workflow\WorkflowEquipmentGroupRelTable;

class EquipmentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new EquipmentService();
        $service->setLogger($container->get('logger'));
        $service->setEquipmentTable($container->get(EquipmentTable::class));
        $service->setEquipmentGroupTable($container->get(EquipmentGroupTable::class));
        $service->setEquipmentGroupRelTable($container->get(EquipmentGroupRelTable::class));
        $service->setWorkflowEquipmentGroupRelTable($container->get(WorkflowEquipmentGroupRelTable::class));
        return $service;
    }
}
