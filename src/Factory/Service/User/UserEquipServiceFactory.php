<?php

namespace Lerp\Equipment\Factory\Service\User;

use Interop\Container\ContainerInterface;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\Equipment\Table\Equipment\EquipmentGroupRelTable;
use Lerp\Equipment\Table\Equipment\EquipmentTable;
use Lerp\Equipment\Table\User\EquipmentUserDetailsTable;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Table\User\ViewUserEquipTable;

class UserEquipServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserEquipService();
        $service->setLogger($container->get('logger'));
        $service->setEquipmentUserTable($container->get(EquipmentUserTable::class));
        $service->setEquipmentUserDetailsTable($container->get(EquipmentUserDetailsTable::class));
        $service->setViewUserEquipTable($container->get(ViewUserEquipTable::class));
        $service->setEquipmentTable($container->get(EquipmentTable::class));
        $service->setEquipmentGroupRelTable($container->get(EquipmentGroupRelTable::class));
        return $service;
    }
}
