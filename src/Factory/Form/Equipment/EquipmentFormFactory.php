<?php

namespace Lerp\Equipment\Factory\Form\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Form\Equipment\EquipmentForm;
use Lerp\Equipment\Table\Equipment\EquipmentGroupTable;
use Lerp\Location\Table\LocationPlaceTable;

class EquipmentFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new EquipmentForm();
        /** @var LocationPlaceTable $locationPlaceTable */
        $locationPlaceTable = $container->get(LocationPlaceTable::class);
        $form->setLocationPlaceUuidAssoc($locationPlaceTable->getLocationPlacesUuidAssoc('stock'));
        /** @var EquipmentGroupTable $equipmentGroupTable */
        $equipmentGroupTable = $container->get(EquipmentGroupTable::class);
        $form->setEquipmentGroupUuids($equipmentGroupTable->getEquipmentGroupsCustom(false, true, false));
        return $form;
    }
}
