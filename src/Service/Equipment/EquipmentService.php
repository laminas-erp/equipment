<?php

namespace Lerp\Equipment\Service\Equipment;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Equipment\Entity\Equipment\EquipmentEntity;
use Lerp\Equipment\Table\Equipment\EquipmentGroupRelTable;
use Lerp\Equipment\Table\Equipment\EquipmentGroupTable;
use Lerp\Equipment\Table\Equipment\EquipmentTable;
use Lerp\Product\Table\Workflow\WorkflowEquipmentGroupRelTable;

class EquipmentService extends AbstractService
{
    protected EquipmentTable $equipmentTable;
    protected EquipmentGroupTable $equipmentGroupTable;
    protected EquipmentGroupRelTable $equipmentGroupRelTable;
    protected WorkflowEquipmentGroupRelTable $workflowEquipmentGroupRelTable;

    public function setEquipmentTable(EquipmentTable $equipmentTable): void
    {
        $this->equipmentTable = $equipmentTable;
    }

    public function setEquipmentGroupTable(EquipmentGroupTable $equipmentGroupTable): void
    {
        $this->equipmentGroupTable = $equipmentGroupTable;
    }

    public function setEquipmentGroupRelTable(EquipmentGroupRelTable $equipmentGroupRelTable): void
    {
        $this->equipmentGroupRelTable = $equipmentGroupRelTable;
    }

    public function setWorkflowEquipmentGroupRelTable(WorkflowEquipmentGroupRelTable $workflowEquipmentGroupRelTable): void
    {
        $this->workflowEquipmentGroupRelTable = $workflowEquipmentGroupRelTable;
    }

    /**
     * @return array Assoc array: [uuid => label, uuid => label].
     */
    public function getEquipmentGroupUuidAssoc(bool $asKeyValObj = false): array
    {
        return $this->equipmentGroupTable->getEquipmentGroupsForEquipment('', $asKeyValObj);
    }

    /**
     * @param string $equipmentUuid
     * @param bool $asKeyValObj
     * @return array
     */
    public function getEquipmentGroupsForEquipment(string $equipmentUuid, bool $asKeyValObj = false): array
    {
        return $this->equipmentGroupTable->getEquipmentGroupsForEquipment($equipmentUuid, $asKeyValObj);
    }

    /**
     * @param array $equipmentGroupUuids
     * @return array EquipmentGroups from all $equipmentGroupUuids
     */
    public function getEquipmentGroupsWithUuids(array $equipmentGroupUuids): array
    {
        return $this->equipmentGroupTable->getEquipmentGroupsWithUuids($equipmentGroupUuids);
    }

    /**
     * @return array FROM view_equipment_group
     */
    public function getEquipmentGroups(): array
    {
        return $this->equipmentGroupTable->getEquipmentGroups();
    }

    /**
     * @param EquipmentEntity $equipmentEntity
     * @return int
     */
    public function updateEquipment(EquipmentEntity $equipmentEntity): int
    {
        $this->equipmentGroupRelTable->deleteEquipmentGroupRelsForEquipment($equipmentEntity->getUuid());
        $this->equipmentGroupRelTable->insertEquipmentGroupRelsForEquipment($equipmentEntity->getUuid(), $equipmentEntity->getEquipmentGroupsCsvAsArray());
        return $this->equipmentTable->updateEquipment($equipmentEntity);
    }

    public function setEquipmentLocked(string $equipmentUuid, bool $locked): bool
    {
        return $this->equipmentTable->updateEquipmentLocked($equipmentUuid, $locked) >= 0;
    }

    public function getEquipmentByUser(string $userUuid): array
    {
        return $this->equipmentTable->getEquipmentByUser($userUuid);
    }

    /**
     * @param string $equipGroupUuid
     * @param string $workflowUuid
     * @return bool
     */
    public function toggleWorkflowEquipGroupRelation(string $equipGroupUuid, string $workflowUuid): bool
    {
        if($this->workflowEquipmentGroupRelTable->existWorkflowEquipmentGroupRel($equipGroupUuid, $workflowUuid)) {
            return $this->workflowEquipmentGroupRelTable->deleteWorkflowEquipmentGroupRel($equipGroupUuid, $workflowUuid);
        } else {
            return !empty($this->workflowEquipmentGroupRelTable->insertWorkflowEquipmentGroupRel($equipGroupUuid, $workflowUuid));
        }
    }
}
