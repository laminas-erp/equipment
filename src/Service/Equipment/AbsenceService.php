<?php

namespace Lerp\Equipment\Service\Equipment;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Equipment\Entity\Equipment\EquipmentAbsenceEntity;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceReasonTable;
use Lerp\Equipment\Table\Equipment\EquipmentAbsenceTable;
use Lerp\Equipment\Table\Equipment\ViewAbsenceTable;

class AbsenceService extends AbstractService
{
    protected EquipmentAbsenceTable $equipmentAbsenceTable;
    protected EquipmentAbsenceReasonTable $equipmentAbsenceReasonTable;
    protected ViewAbsenceTable $viewAbsenceTable;

    public function setEquipmentAbsenceTable(EquipmentAbsenceTable $equipmentAbsenceTable): void
    {
        $this->equipmentAbsenceTable = $equipmentAbsenceTable;
    }

    public function setEquipmentAbsenceReasonTable(EquipmentAbsenceReasonTable $equipmentAbsenceReasonTable): void
    {
        $this->equipmentAbsenceReasonTable = $equipmentAbsenceReasonTable;
    }

    public function setViewAbsenceTable(ViewAbsenceTable $viewAbsenceTable): void
    {
        $this->viewAbsenceTable = $viewAbsenceTable;
    }

    /**
     * @return array
     */
    public function getAbsenceReasons(): array
    {
        return $this->equipmentAbsenceReasonTable->getEquipmentAbsenceReasons();
    }

    /**
     * @param EquipmentAbsenceEntity $equipmentAbsenceEntity
     * @return string
     */
    public function insertAbsence(EquipmentAbsenceEntity $equipmentAbsenceEntity): string
    {
        return $this->equipmentAbsenceTable->insertEquipmentAbsence($equipmentAbsenceEntity);
    }

    /**
     * @param string $equipmentUuid
     * @return array All from db.view_absence for an equipment.
     */
    public function getAbsencesForEquipment(string $equipmentUuid): array
    {
        return $this->viewAbsenceTable->getAbsencesForEquipment($equipmentUuid);
    }

    /**
     * @param string $equipmentAbsenceUuid
     * @return int
     */
    public function deleteEquipmentAbsence(string $equipmentAbsenceUuid): int
    {
        return $this->equipmentAbsenceTable->deleteEquipmentAbsence($equipmentAbsenceUuid);
    }

    /**
     * @param \Lerp\Equipment\Entity\Equipment\EquipmentAbsenceEntity $equipmentAbsenceEntity
     * @return int
     */
    public function updateAbsence(EquipmentAbsenceEntity $equipmentAbsenceEntity): int
    {
        return $this->equipmentAbsenceTable->updateEquipmentAbsence($equipmentAbsenceEntity);
    }
}
