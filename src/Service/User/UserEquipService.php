<?php

namespace Lerp\Equipment\Service\User;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Equipment\Entity\ParamsUserEquip;
use Lerp\Equipment\Entity\User\UserDetailsEntity;
use Lerp\Equipment\Table\Equipment\EquipmentGroupRelTable;
use Lerp\Equipment\Table\Equipment\EquipmentTable;
use Lerp\Equipment\Table\User\EquipmentUserDetailsTable;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Lerp\Equipment\Table\User\ViewUserEquipTable;

class UserEquipService extends AbstractService
{
    protected EquipmentUserTable $equipmentUserTable;
    protected EquipmentUserDetailsTable $equipmentUserDetailsTable;
    protected ViewUserEquipTable $viewUserEquipTable;
    protected EquipmentTable $equipmentTable;
    protected EquipmentGroupRelTable $equipmentGroupRelTable;
    protected int $count = 0;

    public function setEquipmentUserTable(EquipmentUserTable $equipmentUserTable): void
    {
        $this->equipmentUserTable = $equipmentUserTable;
    }

    public function setEquipmentUserDetailsTable(EquipmentUserDetailsTable $equipmentUserDetailsTable): void
    {
        $this->equipmentUserDetailsTable = $equipmentUserDetailsTable;
    }

    public function setViewUserEquipTable(ViewUserEquipTable $viewUserEquipTable): void
    {
        $this->viewUserEquipTable = $viewUserEquipTable;
    }

    public function setEquipmentTable(EquipmentTable $equipmentTable): void
    {
        $this->equipmentTable = $equipmentTable;
    }

    public function setEquipmentGroupRelTable(EquipmentGroupRelTable $equipmentGroupRelTable): void
    {
        $this->equipmentGroupRelTable = $equipmentGroupRelTable;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param string $term
     * @param bool $onlyActive
     * @param ParamsUserEquip $paramsUser
     * @param bool $onlyEquip
     * @return array
     */
    public function searchUserEquip(string $term, bool $onlyActive, ParamsUserEquip $paramsUser, bool $onlyEquip = false): array
    {
        $paramsUser->setDoCount(true);
        $this->count = intval($this->viewUserEquipTable->searchUser($term, $onlyActive, $paramsUser, $onlyEquip)[0]);
        $paramsUser->setDoCount(false);
        return $this->viewUserEquipTable->searchUser($term, $onlyActive, $paramsUser, $onlyEquip);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserByUuid(string $userUuid): array
    {
        return $this->equipmentUserTable->getUser($userUuid);
    }

    /**
     * @param ParamsUserEquip $paramsUserEquip
     * @return array
     */
    public function getUsers(ParamsUserEquip $paramsUserEquip): array
    {
        return $this->equipmentUserTable->getUsers($paramsUserEquip);
    }

    /**
     * @param string $userGroupAlias
     * @return array
     */
    public function getUsersByGroupAlias(string $userGroupAlias): array
    {
        return $this->equipmentUserTable->getUsersByGroupAlias($userGroupAlias);
    }

    public function getUsersByRightAlias(string $userRightAlias): array
    {
        return $this->equipmentUserTable->getUsersByRightAlias($userRightAlias);
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserEquipByUuid(string $userUuid): array
    {
        return $this->viewUserEquipTable->getUserEquip($userUuid);
    }

    /**
     * @param string $userUuid
     * @return string
     */
    public function getEquipmentUuidForUser(string $userUuid): string
    {
        if (empty($userEquip = $this->getUserEquipByUuid($userUuid))) {
            return '';
        }
        return $userEquip['equipment_uuid'];
    }

    /**
     * @param string $userUuid
     * @param float $pricePerHour
     * @param string $locationPlaceUuid
     * @param array $equipGroups An array with equipment_group_uuids.
     * @param int $equipNo
     * @return string The new generated equipment_uuid.
     */
    public function createEquipmentFromUser(string $userUuid, float $pricePerHour, string $locationPlaceUuid, array $equipGroups, int $equipNo): string
    {
        $conn = $this->beginTransaction($this->equipmentTable);
        if (empty($equipmentUuid = $this->equipmentTable->insertEquipment($pricePerHour, $locationPlaceUuid, $equipNo))) {
            $conn->rollback();
            return '';
        }
        foreach ($equipGroups as $equipGroupUuid) {
            if (empty($this->equipmentGroupRelTable->insertEquipmentGroupRel($equipmentUuid, $equipGroupUuid))) {
                $conn->rollback();
                return '';
            }
        }
        if ($this->equipmentUserTable->updateEquipmentUuid($userUuid, $equipmentUuid) < 1) {
            $conn->rollback();
            return '';
        }
        $conn->commit();
        return $equipmentUuid;
    }

    /**
     * @param UserDetailsEntity $userDetailsEntity
     * @return bool
     */
    public function updateOrInsertUserDetails(UserDetailsEntity $userDetailsEntity): bool
    {
        if (empty($userUuid = $userDetailsEntity->getUserUuid())) {
            return false;
        }
        if (!$this->equipmentUserDetailsTable->existUserDetails($userUuid)) {
            return $this->equipmentUserDetailsTable->insertUserDetails($userDetailsEntity);
        }
        return $this->equipmentUserDetailsTable->updateUserDetails($userDetailsEntity) >= 0;
    }

    /**
     * @param ParamsUserEquip $paramsUserEquip
     * @return array
     */
    public function getUsersEquip(ParamsUserEquip $paramsUserEquip): array
    {
        return $this->viewUserEquipTable->getUsersEquip($paramsUserEquip);
    }

    /**
     * @param string $equipmentKey
     * @return array
     */
    public function getEquipmentByKey(string $equipmentKey): array
    {
        return $this->viewUserEquipTable->getEquipmentByKey($equipmentKey);
    }
}
