<?php

namespace Lerp\Equipment\Controller\Rest\User;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Entity\ParamsUserEquip;
use Lerp\Equipment\Service\User\UserEquipService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;

class UserEquipRestController extends AbstractUserRestController
{
    protected UserEquipService $equipmentUserService;

    public function setEquipmentUserService(UserEquipService $equipmentUserService): void
    {
        $this->equipmentUserService = $equipmentUserService;
    }

    /**
     * Create equipment from user.
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = new Uuid();
        if (
            !isset($data['user_uuid'])
            || !isset($data['price_per_hour'])
            || !isset($data['location_place_uuid'])
            || !isset($data['equipment_group_uuid'])
            || !$uuid->isValid($data['user_uuid'])
            || !$uuid->isValid($data['location_place_uuid'])
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $userUuid = filter_var($data['user_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $equipNo = (int)filter_var($data['equip_no'], FILTER_SANITIZE_NUMBER_INT);
        $pricePerHour = filter_var($data['price_per_hour'], FILTER_SANITIZE_NUMBER_FLOAT);
        $lPlaceUuid = filter_var($data['location_place_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $equipGroups = filter_var_array($data, ['equipment_group_uuid' => ['filter' => FILTER_UNSAFE_RAW, 'flags' => FILTER_REQUIRE_ARRAY | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH]]);
        $equipmentGroups = [];
        foreach ($equipGroups['equipment_group_uuid'] as $equipGroup) {
            if ($uuid->isValid($equipGroup)) {
                $equipmentGroups[] = $equipGroup;
            }
        }
        if (empty($equipmentGroups)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($equipmentUuid = $this->equipmentUserService->createEquipmentFromUser($userUuid, $pricePerHour, $lPlaceUuid, $equipmentGroups, $equipNo))) {
            $jsonModel->setUuid($equipmentUuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * GET Search users.
     *
     * @return JsonModel User data with user_details data.
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $filterString = new FilterChainStringSanitize();
        $term = $filterString->filter($request->getQuery('term', ''));
        $onlyEquip = $request->getQuery('only_equip') == 'true';
        $paramsUser = new ParamsUserEquip();
        $paramsUser->setFromParamsArray($request->getQuery()->toArray());

        $jsonModel->setArr($this->equipmentUserService->searchUserEquip($term, false, $paramsUser, $onlyEquip));
        $jsonModel->setCount($this->equipmentUserService->getCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->equipmentUserService->getUserEquipByUuid($id));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
