<?php

namespace Lerp\Equipment\Controller\Rest\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Entity\User\UserFormEntity;
use Bitkorn\User\Form\User\UserForm;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Entity\Equipment\EquipmentEntity;
use Lerp\Equipment\Entity\ParamsUserEquip;
use Lerp\Equipment\Entity\User\UserDetailsEntity;
use Lerp\Equipment\Form\Equipment\EquipmentForm;
use Lerp\Equipment\Form\Equipment\UserDetailsForm;
use Lerp\Equipment\Service\Equipment\EquipmentService;
use Lerp\Equipment\Service\User\UserEquipService;

class EquipmentUserRestController extends AbstractUserRestController
{
    protected UserForm $userForm;
    protected UserDetailsForm $userDetailsForm;
    protected EquipmentForm $equipmentForm;
    protected UserEquipService $userEquipService;
    protected EquipmentService $equipmentService;

    public function setUserForm(UserForm $userForm): void
    {
        $this->userForm = $userForm;
    }

    public function setUserDetailsForm(UserDetailsForm $userDetailsForm): void
    {
        $this->userDetailsForm = $userDetailsForm;
    }

    public function setEquipmentForm(EquipmentForm $equipmentForm): void
    {
        $this->equipmentForm = $equipmentForm;
    }

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * Update a complete EquipmentUser.
     * 'complete' means database tables:
     * - user
     * - user_details
     * - equipment & equipment_group_rel
     *
     * @param string $id equipment_uuid
     * @param array $data user_uuid must be present.
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $formValid = true;
        $this->userForm->setPrimaryKeyAvailable(true);
        $this->userForm->init();
        $this->userForm->setData($data);
        if (!$this->userForm->isValid()) {
            $jsonModel->addMessages($this->userForm->getMessages());
            $formValid = false;
        }
        $this->userDetailsForm->setPrimaryKeyAvailable(true);
        $this->userDetailsForm->init();
        $this->userDetailsForm->setData($data);
        if (!$this->userDetailsForm->isValid()) {
            $jsonModel->addMessages($this->userDetailsForm->getMessages());
            $formValid = false;
        }
        $this->equipmentForm->setPrimaryKeyAvailable(true);
        $this->equipmentForm->init();
        $this->equipmentForm->setData($data);
        if (!$this->equipmentForm->isValid()) {
            $jsonModel->addMessages($this->equipmentForm->getMessages());
            $formValid = false;
        }
        if (!$formValid) {
            return $jsonModel;
        }
        $userFormEntity = new UserFormEntity();
        if (!$userFormEntity->exchangeArrayFromDatabase($this->userForm->getData())
            || !$this->userService->updateUserWithUserFormEntity($userFormEntity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $userDetailsEntity = new UserDetailsEntity();
        if (!$userDetailsEntity->exchangeArrayFromDatabase($this->userDetailsForm->getData())
            || !$this->userEquipService->updateOrInsertUserDetails($userDetailsEntity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $equipmentEntity = new EquipmentEntity();
        if (!$equipmentEntity->exchangeArrayFromDatabase($this->equipmentForm->getData())
            || !$this->equipmentService->updateEquipment($equipmentEntity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $params = new ParamsUserEquip();
        $params->setFromParamsArray($this->params()->fromQuery());
        $jsonModel->setArr($this->userEquipService->getUsersEquip($params));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
