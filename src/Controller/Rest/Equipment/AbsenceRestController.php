<?php

namespace Lerp\Equipment\Controller\Rest\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Entity\Equipment\EquipmentAbsenceEntity;
use Lerp\Equipment\Form\Equipment\AbsenceForm;
use Lerp\Equipment\Service\Equipment\AbsenceService;

class AbsenceRestController extends AbstractUserRestController
{
    protected AbsenceService $absenceService;
    protected AbsenceForm $absenceForm;

    public function setAbsenceService(AbsenceService $absenceService): void
    {
        $this->absenceService = $absenceService;
    }

    public function setAbsenceForm(AbsenceForm $absenceForm): void
    {
        $this->absenceForm = $absenceForm;
    }

    /**
     * Create an equipment absence.
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->absenceForm->init();
        $this->absenceForm->setData($data);
        if (!$this->absenceForm->isValid()) {
            $jsonModel->addMessages($this->absenceForm->getMessages());
            return $jsonModel;
        }
        $entity = new EquipmentAbsenceEntity();
        $entity->exchangeArrayFromDatabase($this->absenceForm->getData());
        if (!empty($uuid = $this->absenceService->insertAbsence($entity))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->absenceService->deleteEquipmentAbsence($id) > 0) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET db.view_absence for equipment_uuid.
     * @param string $id equipment_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->absenceService->getAbsencesForEquipment($id));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id equipment_absence_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->absenceForm->init();
        $this->absenceForm->setData($data);
        if (!$this->absenceForm->isValid()) {
            $jsonModel->addMessages($this->absenceForm->getMessages());
            return $jsonModel;
        }
        $entity = new EquipmentAbsenceEntity();
        $entity->exchangeArrayFromDatabase($this->absenceForm->getData());
        $entity->setUuid($id);
        if ($this->absenceService->updateAbsence($entity) >= 0) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
