<?php

namespace Lerp\Equipment\Controller\Ajax\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Service\Equipment\EquipmentService;

class EquipmentGroupAjaxController extends AbstractUserController
{
    protected EquipmentService $equipmentService;

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * @return JsonModel
     */
    public function toggleWorkflowRelationAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $equipGroupUuid = filter_var($this->params('equip_group_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $workflowUuid = filter_var($this->params('workflow_uuid', ''), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuid = new Uuid();
        if (!$uuid->isValid($equipGroupUuid) || !$uuid->isValid($workflowUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessage('equip_group_uuid AND workflow_uuid must be provided');
            return $jsonModel;
        }
        if ($this->equipmentService->toggleWorkflowEquipGroupRelation($equipGroupUuid, $workflowUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
