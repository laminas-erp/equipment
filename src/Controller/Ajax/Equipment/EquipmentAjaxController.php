<?php

namespace Lerp\Equipment\Controller\Ajax\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Service\Equipment\EquipmentService;

class EquipmentAjaxController extends AbstractUserController
{
    protected EquipmentService $equipmentService;

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * @return JsonModel
     */
    public function userEquipmentGroupsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($equipUuid = $this->params('equip_uuid', '')) || !(new Uuid())->isValid($equipUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->equipmentService->getEquipmentGroupsForEquipment($equipUuid, true));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

}
