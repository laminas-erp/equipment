<?php

namespace Lerp\Equipment\Controller\Ajax\Equipment;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Equipment\Service\Equipment\EquipmentService;

class EquipmentListsAjaxController extends AbstractUserController
{
    protected EquipmentService $equipmentService;

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * @return JsonModel
     */
    public function equipmentGroupAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $asKeyValObj = filter_var($this->params()->fromQuery('asKeyValObj', false), FILTER_VALIDATE_BOOLEAN);
        $groups = $this->equipmentService->getEquipmentGroupUuidAssoc($asKeyValObj);
        if ($asKeyValObj) {
            $jsonModel->setKeyValObjArr($groups);
        } else {
            $jsonModel->setObj($groups);
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function equipmentGroupsWithUuidsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $equipGroupUuids = explode(',', filter_var($this->params('uuids'), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]));
        if (empty($equipGroupUuids) || !is_array($equipGroupUuids)) {
            return $jsonModel;
        }
        $uuid = new Uuid();
        foreach ($equipGroupUuids as $equipGroupUuid) {
            if(!$uuid->isValid($equipGroupUuid)) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                $jsonModel->addMessage('EquipmentGroupUuid invalid');
                return $jsonModel;
            }
        }
        $jsonModel->setArr($this->equipmentService->getEquipmentGroupsWithUuids($equipGroupUuids));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function equipmentGroupsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->equipmentService->getEquipmentGroups());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
