<?php

namespace Lerp\Equipment\Controller\Ajax\User;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Lerp\Equipment\Entity\ParamsUserEquip;
use Lerp\Equipment\Service\User\UserEquipService;

class EquipmentUserController extends AbstractUserController
{
    protected UserEquipService $userEquipService;

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    /**
     * @return JsonModel
     */
    public function meUserDetailsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->userEquipService->getUserByUuid($this->userService->getUserUuid()));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function usersAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $paramsUser = new ParamsUserEquip();
        $paramsUser->setFromParamsArray($request->getQuery()->toArray());
        $jsonModel->setArr($this->userEquipService->getUsers($paramsUser));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
