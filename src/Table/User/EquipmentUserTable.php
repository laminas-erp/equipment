<?php

namespace Lerp\Equipment\Table\User;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Equipment\Entity\ParamsUserEquip;

/**
 * Class EquipmentUserTable
 * @package Lerp\Equipment\Table\User
 *
 * Extended the table in module bitkorn/user.
 * ...special queries for table user_details
 * ...special queries for equipment
 *
 */
class EquipmentUserTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'user';
    protected array $userColumns = ['user_uuid', 'user_login', 'user_active', 'user_time_create', 'user_email', 'user_time_update', 'equipment_uuid', 'user_lang_iso'];
    protected array $userDetailsColumns = ['user_details_name_first', 'user_details_name_last', 'user_details_tel_land', 'user_details_tel_mobile', 'user_details_tel_fax', 'user_details_brand_pre', 'user_details_brand_post', 'user_details_job'];

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUser(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns($this->userColumns);
            $select->join('user_details', 'user_details.user_uuid = user.user_uuid'
                , $this->userDetailsColumns
                , Select::JOIN_LEFT);
            $select->where(['user.user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getUsers(ParamsUserEquip $paramsUserEquip): array
    {
        $select = $this->sql->select();
        try {
            $select->columns($this->userColumns);
            $select->join('user_details', 'user_details.user_uuid = user.user_uuid'
                , $this->userDetailsColumns
                , Select::JOIN_LEFT);
            $paramsUserEquip->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getUsersByGroupAlias(string $userGroupAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->columns($this->userColumns);
            $select->join('user_details', 'user_details.user_uuid = user.user_uuid'
                , $this->userDetailsColumns
                , Select::JOIN_LEFT);

            $selectGroup = new Select('user_group');
            $selectGroup->columns(['user_group_uuid']);
            $selectGroup->where(['user_group_alias' => $userGroupAlias]);

            $selectRelation = new Select('user_group_relation');
            $selectRelation->columns(['user_uuid']);
            $selectRelation->where->in('user_group_uuid', $selectGroup);

            $select->where->in('user.user_uuid', $selectRelation);
            $select->order('user_time_create');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getUsersByRightAlias(string $userRightAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->columns($this->userColumns);
            $select->join('user_details', 'user_details.user_uuid = user.user_uuid'
                , $this->userDetailsColumns
                , Select::JOIN_LEFT);

            $selectRight = new Select('user_right');
            $selectRight->columns(['user_right_id']);
            $selectRight->where(['user_right_alias' => $userRightAlias]);

            $selectRelation = new Select('user_right_relation');
            $selectRelation->columns(['user_uuid']);
            $selectRelation->where->in('user_right_id', $selectRight);

            $select->where->in('user.user_uuid', $selectRelation);
            $select->order('user_time_create');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $term
     * @param bool $onlyActive
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @param bool $doCount
     * @return array
     */
    public function searchUser(string $term, bool $onlyActive, string $orderField, string $orderDirec, int $offset = 0, int $limit = 0, bool $doCount = false): array
    {
        $select = $this->sql->select();
        try {
            if ($doCount) {
                $select->columns(['count_user' => new Expression('COUNT(user_uuid)')]);
            } else {
                $select->columns($this->userColumns);
                $select->join('user_details', 'user_details.user_uuid = user.user_uuid'
                    , $this->userDetailsColumns
                    , Select::JOIN_LEFT);
            }

            if (!empty($term)) {
                $select->where
                    ->like('user_login', '%' . $term . '%')
                    ->or->like('user_email', '%' . $term . '%')
                    ->or->like('user_details_name_first', '%' . $term . '%')
                    ->or->like('user_details_name_last', '%' . $term . '%');
            }

            if ($onlyActive) {
                $select->where(['user_active' => 1]);
            }

            if (!$doCount && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }

            if (!$doCount && (!empty($offset) || !empty($limit))) {
                $select->limit($limit)->offset($offset);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($doCount) {
                    return $result->current()->getArrayCopy();
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateEquipmentUuid(string $userUuid, string $equipmentUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'equipment_uuid' => $equipmentUuid
            ]);
            $update->where(['user_uuid' => $userUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
