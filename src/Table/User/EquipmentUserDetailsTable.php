<?php

namespace Lerp\Equipment\Table\User;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Equipment\Entity\User\UserDetailsEntity;

class EquipmentUserDetailsTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'user_details';

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserDetails(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existUserDetails(string $userUuid): bool
    {
        return !empty($this->getUserDetails($userUuid));
    }

    /**
     * @param UserDetailsEntity $userDetailsEntity
     * @return bool
     */
    public function insertUserDetails(UserDetailsEntity $userDetailsEntity): bool
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'user_uuid'               => $userDetailsEntity->getUserUuid(),
                'user_details_name_first' => $userDetailsEntity->getUserDetailsNameFirst(),
                'user_details_name_last'  => $userDetailsEntity->getUserDetailsNameLast(),
                'user_details_tel_land'   => $userDetailsEntity->getUserDetailsTelLand(),
                'user_details_tel_mobile' => $userDetailsEntity->getUserDetailsTelMobile(),
                'user_details_tel_fax'    => $userDetailsEntity->getUserDetailsTelFax(),
                'user_details_brand_pre'  => $userDetailsEntity->getUserDetailsBrandPre(),
                'user_details_brand_post' => $userDetailsEntity->getUserDetailsBrandPost(),
                'user_details_job'        => $userDetailsEntity->getUserDetailsJob(),
            ]);
            return $this->insertWith($insert) == 1;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function deleteUserDetails(string $userUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['user_uuid' => $userUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param UserDetailsEntity $userDetailsEntity
     * @return int
     */
    public function updateUserDetails(UserDetailsEntity $userDetailsEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'user_details_name_first' => $userDetailsEntity->getUserDetailsNameFirst(),
                'user_details_name_last'  => $userDetailsEntity->getUserDetailsNameLast(),
                'user_details_tel_land'   => $userDetailsEntity->getUserDetailsTelLand(),
                'user_details_tel_mobile' => $userDetailsEntity->getUserDetailsTelMobile(),
                'user_details_tel_fax'    => $userDetailsEntity->getUserDetailsTelFax(),
                'user_details_brand_pre'  => $userDetailsEntity->getUserDetailsBrandPre(),
                'user_details_brand_post' => $userDetailsEntity->getUserDetailsBrandPost(),
                'user_details_job'        => $userDetailsEntity->getUserDetailsJob(),
            ]);
            $update->where(['user_uuid' => $userDetailsEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
