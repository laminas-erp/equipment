<?php

namespace Lerp\Equipment\Table\User;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Equipment\Entity\ParamsUserEquip;

class ViewUserEquipTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_user_equip';

    /**
     * @param string $userUuid
     * @return array
     */
    public function getUserEquip(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['user_uuid' => $userUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getUsersEquip(ParamsUserEquip $paramsUserEquip): array
    {
        $select = $this->sql->select();
        try {
            if($paramsUserEquip->isOnlyEquip()) {
                $select->where->isNotNull('equipment_uuid');
            }
            $paramsUserEquip->computeSelect($select, 'user_details_name_last ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function searchUser(string $term, bool $onlyActive, ParamsUserEquip $paramsUser, bool $onlyEquip = false): array
    {
        $select = $this->sql->select();
        try {
            if ($paramsUser->isDoCount()) {
                $select->columns(['count_user' => new Expression('COUNT(user_uuid)')]);
            }
            $paramsUser->computeSelect($select, 'user_login ASC');
            if (!empty($term)) {
                $select->where
                    ->like('user_login', '%' . $term . '%')
                    ->or->like('user_email', '%' . $term . '%')
                    ->or->like('user_details_name_first', '%' . $term . '%')
                    ->or->like('user_details_name_last', '%' . $term . '%');
            }
            if ($onlyActive) {
                $select->where(['user_active' => 1]);
            }
            if ($onlyEquip) {
                $select->where->isNotNull('equipment_uuid');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($paramsUser->isDoCount()) {
                    return [intval($result->current()['count_user'])];
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [0];
    }

    /**
     * @param string $equipmentKey
     * @return array
     */
    public function getEquipmentByKey(string $equipmentKey): array
    {
        $select = $this->sql->select();
        try {
            $select->where->equalTo('equipment_key', $equipmentKey);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
