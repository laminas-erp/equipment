<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Equipment\Entity\Equipment\EquipmentAbsenceEntity;

class EquipmentAbsenceTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'equipment_absence';

    /**
     * @param string $equipmentAbsenceUuid
     * @return array
     */
    public function getEquipmentAbsence(string $equipmentAbsenceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_absence_uuid' => $equipmentAbsenceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertEquipmentAbsence(EquipmentAbsenceEntity $equipmentAbsenceEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'equipment_absence_uuid'        => $uuid,
                'equipment_uuid'                => $equipmentAbsenceEntity->getEquipmentUuid(),
                'equipment_absence_start'       => $equipmentAbsenceEntity->getEquipmentAbsenceStart(),
                'equipment_absence_end'         => $equipmentAbsenceEntity->getEquipmentAbsenceEnd(),
                'equipment_absence_reason_uuid' => $equipmentAbsenceEntity->getEquipmentAbsenceReasonUuid(),
                'equipment_absence_subject'     => $equipmentAbsenceEntity->getEquipmentAbsenceSubject(),
                'equipment_absence_text'        => $equipmentAbsenceEntity->getEquipmentAbsenceText(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $equipmentAbsenceUuid
     * @return int
     */
    public function deleteEquipmentAbsence(string $equipmentAbsenceUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['equipment_absence_uuid' => $equipmentAbsenceUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param \Lerp\Equipment\Entity\Equipment\EquipmentAbsenceEntity $equipmentAbsenceEntity
     * @return int
     */
    public function updateEquipmentAbsence(EquipmentAbsenceEntity $equipmentAbsenceEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'equipment_uuid'                => $equipmentAbsenceEntity->getEquipmentUuid(),
                'equipment_absence_start'       => $equipmentAbsenceEntity->getEquipmentAbsenceStart(),
                'equipment_absence_end'         => $equipmentAbsenceEntity->getEquipmentAbsenceEnd(),
                'equipment_absence_reason_uuid' => $equipmentAbsenceEntity->getEquipmentAbsenceReasonUuid(),
                'equipment_absence_subject'     => $equipmentAbsenceEntity->getEquipmentAbsenceSubject(),
                'equipment_absence_text'        => $equipmentAbsenceEntity->getEquipmentAbsenceText(),
            ]);
            $update->where(['equipment_absence_uuid' => $equipmentAbsenceEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
