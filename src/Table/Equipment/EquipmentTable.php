<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Equipment\Entity\Equipment\EquipmentEntity;

class EquipmentTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'equipment';

    /**
     * @param string $equipmentUuid
     * @return array
     */
    public function getEquipment(string $equipmentUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_uuid' => $equipmentUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param float $pricePerHour
     * @param string $locationPlaceUuid
     * @param int $equipNo Saved only if it is not empty.
     * @return string The new generated equipment_uuid.
     */
    public function insertEquipment(float $pricePerHour, string $locationPlaceUuid, int $equipNo): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $values = [
            'equipment_uuid'           => $uuid,
            'equipment_price_per_hour' => $pricePerHour,
            'location_place_uuid'      => $locationPlaceUuid,
        ];
        if ($equipNo) {
            $values['equipment_no'] = $equipNo;
        }
        try {
            $insert->values($values);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateEquipment(EquipmentEntity $equipmentEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'equipment_no'             => $equipmentEntity->getEquipmentNo(),
                'equipment_price_per_hour' => $equipmentEntity->getEquipmentPricePerHour(),
                'location_place_uuid'      => $equipmentEntity->getLocationPlaceUuid(),
            ]);
            $update->where(['equipment_uuid' => $equipmentEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateEquipmentLocked(string $equipmentUuid, bool $locked): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['equipment_locked' => $locked])
                ->where(['equipment_uuid' => $equipmentUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $userUuid
     * @return array
     */
    public function getEquipmentByUser(string $userUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectUser = new Select('user');
            $selectUser->columns(['equipment_uuid']);
            $selectUser->where->equalTo('user_uuid', $userUuid);
            /** @var HydratingResultSet $userResult */
            $userResult = $this->selectWith($selectUser);
            if (!$userResult->valid() || $userResult->count() != 1 || empty($user = $userResult->toArray()[0]) || empty($user['equipment_uuid'])) {
                return [];
            }
            $select->where->equalTo('equipment_uuid', $user['equipment_uuid']);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
