<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class EquipmentGroupRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'equipment_group_rel';

    /**
     * @param string $equipmentGroupRelUuid
     * @return array
     */
    public function getEquipmentGroupRel(string $equipmentGroupRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_group_rel_uuid' => $equipmentGroupRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $equipmentUuid
     * @param string $equipmentGroupUuid
     * @return string
     */
    public function insertEquipmentGroupRel(string $equipmentUuid, string $equipmentGroupUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'equipment_group_rel_uuid' => $uuid,
                'equipment_uuid'           => $equipmentUuid,
                'equipment_group_uuid'     => $equipmentGroupUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteEquipmentGroupRelsForEquipment(string $equipmentUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['equipment_uuid' => $equipmentUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertEquipmentGroupRelsForEquipment(string $equipmentUuid, array $equipmentGroupUuids): int
    {
        if (empty($equipmentGroupUuids)) {
            return 0;
        }
        $insert = $this->sql->insert();
        try {
            $c = 0;
            foreach ($equipmentGroupUuids as $equipmentGroupUuid) {
                $uuid = $this->uuid();
                $insert->values([
                    'equipment_group_rel_uuid' => $uuid,
                    'equipment_uuid'           => $equipmentUuid,
                    'equipment_group_uuid'     => $equipmentGroupUuid,
                ]);
                $c += $this->insertWith($insert);
            }
            return $c;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
