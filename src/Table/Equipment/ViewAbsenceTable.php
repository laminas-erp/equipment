<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewAbsenceTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_absence';

    /**
     * @param string $equipmentUuid
     * @return array
     */
    public function getAbsencesForEquipment(string $equipmentUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_uuid' => $equipmentUuid]);
            $select->order('equipment_absence_start DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
