<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class EquipmentGroupTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'equipment_group';

    /**
     * @param string $equipmentGroupUuid
     * @return array
     */
    public function getEquipmentGroup(string $equipmentGroupUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['equipment_group_uuid' => $equipmentGroupUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array FROM view_equipment_group
     */
    public function getEquipmentGroups(): array
    {
        $select = new Select('view_equipment_group');
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param bool $asKeyValObj IF TRUE THEN [{key:'the uuid',value:'the label'},{}]
     * @param bool $onlyUuids IF TRUE THEN ['an uuid', 'another uuid', []]
     * @param bool $asUuidAssoc IF TRUE THEN
     * @return array If are all params FALSE THEN it is a flat list of EquipmentGroups.
     */
    public function getEquipmentGroupsCustom(bool $asKeyValObj = false, bool $onlyUuids = false, bool $asUuidAssoc = false): array
    {
        $select = $this->sql->select();
        $returnArr = [];
        try {
            $select->order('equipment_group_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                foreach ($result->toArray() as $row) {
                    if ($asKeyValObj) {
                        $returnArr[] = ['key' => $row['equipment_group_uuid'], 'val' => $row['equipment_group_label']];
                    } else if ($onlyUuids) {
                        $returnArr[] = $row['equipment_group_uuid'];
                    } else if ($asUuidAssoc) {
                        $returnArr[$row['equipment_group_uuid']] = $row['equipment_group_label'];
                    } else {
                        $returnArr[] = $row;
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $returnArr;
    }

    /**
     * @param string $equipmentUuid
     * @param bool $asKeyValObj
     * @return array
     */
    public function getEquipmentGroupsForEquipment(string $equipmentUuid = '', bool $asKeyValObj = false): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            if ($equipmentUuid) {
                $selectRel = new Select('equipment_group_rel');
                $selectRel->columns(['equipment_group_uuid']);
                $selectRel->where(['equipment_uuid' => $equipmentUuid]);
                $select->where->in('equipment_group_uuid', $selectRel);
            }
            $select->order('equipment_group_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    if (!$asKeyValObj) {
                        $uuidAssoc[$row['equipment_group_uuid']] = $row['equipment_group_label'];
                    } else {
                        $uuidAssoc[] = ['key' => $row['equipment_group_uuid'], 'val' => $row['equipment_group_label']];
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }

    public function getEquipmentGroupsWithUuids(array $equipmentGroupUuids): array
    {
        $select = $this->sql->select();
        try {
            $select->where->in('equipment_group_uuid', $equipmentGroupUuids);
            $select->order('equipment_group_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
