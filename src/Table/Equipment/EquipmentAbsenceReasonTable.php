<?php

namespace Lerp\Equipment\Table\Equipment;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class EquipmentAbsenceReasonTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'equipment_absence_reason';

    /**
     * @return array
     */
    public function getEquipmentAbsenceReasons(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('equipment_absence_reason_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
