<?php

namespace Lerp\Equipment\Form\Equipment;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class UserDetailsForm extends AbstractForm implements InputFilterProviderInterface
{

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'user_uuid']);
            $this->add(['name' => 'equipment_uuid']);
        }
        $this->add(['name' => 'user_details_name_first']);
        $this->add(['name' => 'user_details_name_last']);
        $this->add(['name' => 'user_details_tel_land']);
        $this->add(['name' => 'user_details_tel_mobile']);
        $this->add(['name' => 'user_details_tel_fax']);
        $this->add(['name' => 'user_details_brand_pre']);
        $this->add(['name' => 'user_details_brand_post']);
        $this->add(['name' => 'user_details_job']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['user_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class,
                    ]
                ]
            ];
            $filter['equipment_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class,
                    ]
                ]
            ];
        }

        $filter['user_details_name_first'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 100,
                    ]
                ]
            ]
        ];

        $filter['user_details_name_last'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 100,
                    ]
                ]
            ]
        ];

        $filter['user_details_tel_land'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 80,
                    ]
                ]
            ]
        ];

        $filter['user_details_tel_mobile'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 80,
                    ]
                ]
            ]
        ];

        $filter['user_details_tel_fax'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 80,
                    ]
                ]
            ]
        ];

        $filter['user_details_brand_pre'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 160,
                    ]
                ]
            ]
        ];

        $filter['user_details_brand_post'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 160,
                    ]
                ]
            ]
        ];

        $filter['user_details_job'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 160,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
