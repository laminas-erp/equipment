<?php

namespace Lerp\Equipment\Form\Equipment;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\Explode;
use Laminas\Validator\InArray;
use Laminas\Validator\Uuid;

class EquipmentForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $locationPlaceUuidAssoc = [];
    protected array $equipmentGroupUuids = [];

    public function setLocationPlaceUuidAssoc(array $locationPlaceUuidAssoc): void
    {
        $this->locationPlaceUuidAssoc = $locationPlaceUuidAssoc;
    }

    public function setEquipmentGroupUuids(array $equipmentGroupUuids): void
    {
        $this->equipmentGroupUuids = $equipmentGroupUuids;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'equipment_uuid']);
        }
        $this->add(['name' => 'equipment_no']);
        $this->add(['name' => 'equipment_price_per_hour']);
        $this->add(['name' => 'location_place_uuid']);
        $this->add(['name' => 'equipment_groups_csv']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        if ($this->primaryKeyAvailable) {
            $filter['equipment_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class,
                    ]
                ]
            ];
        }

        $filter['equipment_no'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Digits::class,
                ]
            ]
        ];

        $filter['equipment_price_per_hour'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        $filter['location_place_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Uuid::class,
                ],
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->locationPlaceUuidAssoc)
                    ]
                ]
            ]
        ];

        $inArray = new InArray();
        $inArray->setHaystack($this->equipmentGroupUuids);
        $filter['equipment_groups_csv'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => Explode::class,
                    'options' => [
                        'validator' => $inArray
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
