<?php

namespace Lerp\Equipment\Form\Equipment;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Date;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class AbsenceForm extends AbstractForm implements InputFilterProviderInterface
{
    protected Adapter $dbAdapter;

    public function setDbAdapter(Adapter $dbAdapter): void
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'equipment_absence_uuid']);
        }
        $this->add(['name' => 'equipment_uuid']);
        $this->add(['name' => 'equipment_absence_start']);
        $this->add(['name' => 'equipment_absence_end']);
        $this->add(['name' => 'equipment_absence_reason_uuid']);
        $this->add(['name' => 'equipment_absence_subject']);
        $this->add(['name' => 'equipment_absence_text']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['equipment_absence_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class],
                ], 'validators' => [
                    [
                        'name' => Uuid::class,
                    ],
                    [
                        'name'    => RecordExists::class,
                        'options' => [
                            'adapter' => $this->dbAdapter,
                            'table'   => 'equipment_absence',
                            'field'   => 'equipment_absence_uuid',
                        ]
                    ]
                ]
            ];
        }

        $filter['equipment_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Uuid::class,
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->dbAdapter,
                        'table'   => 'equipment',
                        'field'   => 'equipment_uuid',
                    ]
                ]
            ]
        ];

        $filter['equipment_absence_start'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Date::class,
                ],
            ]
        ];

        $filter['equipment_absence_end'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Date::class,
                ],
            ]
        ];

        $filter['equipment_absence_reason_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => Uuid::class,
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->dbAdapter,
                        'table'   => 'equipment_absence_reason',
                        'field'   => 'equipment_absence_reason_uuid',
                    ]
                ]
            ]
        ];

        $filter['equipment_absence_subject'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 160,
                    ]
                ]
            ]
        ];

        $filter['equipment_absence_text'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
